# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'stats.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(470, 356)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 2)
        self.view = QtWidgets.QPushButton(self.centralwidget)
        self.view.setObjectName("view")
        self.gridLayout.addWidget(self.view, 0, 2, 1, 1)
        self.mode = QtWidgets.QComboBox(self.centralwidget)
        self.mode.setObjectName("mode")
        self.gridLayout.addWidget(self.mode, 1, 0, 1, 1)
        self.stats = QtWidgets.QTableWidget(self.centralwidget)
        self.stats.setObjectName("stats")
        self.stats.setColumnCount(0)
        self.stats.setRowCount(0)
        self.gridLayout.addWidget(self.stats, 1, 1, 4, 2)
        self.dateStart = QtWidgets.QDateEdit(self.centralwidget)
        self.dateStart.setObjectName("dateStart")
        self.gridLayout.addWidget(self.dateStart, 2, 0, 1, 1)
        self.dateEnd = QtWidgets.QDateEdit(self.centralwidget)
        self.dateEnd.setObjectName("dateEnd")
        self.gridLayout.addWidget(self.dateEnd, 3, 0, 1, 1)
        self.site = QtWidgets.QComboBox(self.centralwidget)
        self.site.setObjectName("site")
        self.gridLayout.addWidget(self.site, 4, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Статистика"))
        self.label.setText(_translate("MainWindow", "Общая статистика"))
        self.view.setText(_translate("MainWindow", "Показать"))

