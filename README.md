# UI Python Desktop #

## What is this repository for?  ##

Windows and Unix app for AnyStat Users

## How install and run for Linux ##

```
#!bash

sudo apt-get install python3.5
sudo apt-get install python3-pip
sudo pip3 install -r requirements.txt
python3 main.py

```

## How install and run for Windows ##
Download and install Python3.5.2 from https://www.python.org/downloads/
```
#!bash
pip3 install -r requirements.txt
python main.py

```

### На данный момент готова первая версия: ###
* Показ общей статистики по выбранному сайту
* Показ ежедневной статистики на данный моменты выводит данные по последним ста страницам