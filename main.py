import sys

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtWebEngineWidgets import *
from anystats.stats import *
import requests
import pygal

SITE_ROOT = 'http://52.37.48.231:8080'
DELMITER = '@@@'


class MyWindow(QMainWindow):
    # Главное окно программы - MainWindow

    def __init__(self):
        # главное окно
        super().__init__()
        self.views = Views()

        # подключение формы
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # косметика
        self.ui.statusbar.showMessage('© AnyStats, 2016')
        self.setWindowIcon(QIcon("anystats/icon.png"))
        self.ui.label.setFixedSize(200, 100)
        self.ui.stats.setMinimumHeight(210)

        # опционал программы
        self.ui.mode.setCurrentIndex(0)
        self.ui.mode.addItems(('Общая статистика'+DELMITER+'Ежедневная статистика').split(DELMITER))

        # генерация списка сайтов из бд
        self.ui.site.setCurrentIndex(0)
        self.ui.site.addItems(Repository().rep_sites().split(DELMITER))

        # отображение интерфейса в соответствии с выбранной опцией
        self.ui.mode.currentIndexChanged.connect(self.show_interface)

        # проще спрятать, чем нарисовать, вызов минимального интерфейса в начале
        self.show_interface()

        # выполнение запросов
        self.ui.view.clicked.connect(self.show_stats)

    def show_interface(self):
        # Меняет интерфейс в зависимости от типа

        # для общей статистики
        if self.ui.mode.currentIndex() == 0:
            self.ui.label.setText("Общая статистика")
            self.ui.dateEnd.hide()
            self.ui.dateStart.hide()

        # для ежедневной статистики
        else:
            self.ui.label.setText("Ежедневная статистика")
            self.ui.dateEnd.show()
            self.ui.dateStart.show()

    def modalWindow(self):
        # Вызывает отдельное окно с графиком
        self.modal = QWebEngineView()
        self.modal.resize(800, 600)
        self.modal.setContent(self.get_xml(), mimeType='text/xml')
        self.modal.show()

    def show_stats(self):
        # Управляющий контроллер
        if self.ui.mode.currentIndex() == 0:
            data = Repository().rep_one_site_person(self.ui.site.currentIndex())
            # data = ['', '']
            # self.modalWindow()
        else:
            data = Repository().rep_ranks_persons_last_pages()

        self.views.fill_table(data)

    def get_xml(self):
        # возвращает xml
        pie_chart = pygal.Pie()
        pie_chart.title = 'Browser usage in February 2012 (in %)'
        pie_chart.add('IE', 19.5)
        pie_chart.add('Firefox', 36.6)
        pie_chart.add('Chrome', 36.3)
        pie_chart.add('Safari', 4.5)
        pie_chart.add('Opera', 2.3)
        return pie_chart.render()


class Repository:
    # Принимает json и возвращает string

    def __init__(self):
        self.request = Request()

    def rep_sites(self):
        # список сайтов
        sites = ''
        sites_list = self.request.get_sites_list()
        for i in range(len(sites_list['content'])):
            sites += '{}{}'.format(sites_list['content'][i]['name'], DELMITER)
        return sites[:-len(DELMITER)]  # Срез убирает последний DELMITER

    def rep_persons(self):
        # список персон
        persons = 'Персона'+DELMITER
        persons_list = self.request.get_persons_list()
        for i in range(persons_list['numberOfElements']):
            persons += '{}{}'.format(persons_list['content'][i]['name'], DELMITER)
        return persons[:-len(DELMITER)]  # Срез убирает последний DELMITER

    def rep_ranks_persons_last_pages(self):
        # подсчет совпадений на последних ста страницах
        persons = 'Персона' + DELMITER
        persons_list = self.request.get_persons_list()
        ranks = 'Совпадения'+DELMITER
        ranks_list = self.request.get_ranks_list()
        for i in range(persons_list['numberOfElements']):
            persons += '{}{}'.format(persons_list['content'][i]['name'], DELMITER)
            rank_sum = 0
            for j in range(ranks_list['numberOfElements']):
                if persons_list['content'][i]['id'] == ranks_list['content'][j]['person']:
                    rank_sum += ranks_list['content'][j]['rank']
            ranks += '{}{}'.format(rank_sum, DELMITER)

        return [self.rep_persons(), ranks[:-len(DELMITER)]]

    def rep_all_site_person(self):
        # общая статистика по всем сайтам
        persons_list = self.request.get_persons_list()
        ranks = 'Совпадения' + DELMITER
        for i in range(persons_list['numberOfElements']):
            rank = self.request.get_total_rank_person(persons_list['content'][i]['id'])['rank']
            ranks += '{}{}'.format(rank, DELMITER)
        return [self.rep_persons(), ranks[:-len(DELMITER)]]

    def rep_one_site_person(self, site_on_list):
        # статистика одного сайта
        persons_list = self.request.get_persons_list()
        ranks = 'Совпадения' + DELMITER
        site_id = self.request.get_sites_list()['content'][site_on_list]['id']
        for i in range(persons_list['numberOfElements']):
            try:
                rank = self.request.get_total_rank_person_on_site(persons_list['content'][i]['id'], site_id)['rank']
            except KeyError:
                rank = 0
            ranks += '{}{}'.format(rank, DELMITER)
        return [self.rep_persons(), ranks[:-len(DELMITER)]]


class Request:
    # send request to REST and return json response

    def __init__(self):
        self.root = SITE_ROOT

    def get_sites_list(self):
        # Список доступных сайтов
        request = '/sites'
        url = self.root + request
        response = requests.get(url)

        return response.json()

    def get_persons_list(self):
        # список персон
        request = '/persons'
        url = self.root + request
        response = requests.get(url)

        return response.json()

    def get_ranks_list(self, options=''):
        # список совпадений
        request = '/ranks'
        url = self.root + request + options
        response = requests.get(url)

        return response.json()

    def get_keywords_list(self):
        # список совпадений
        request = '/keywords'
        url = self.root + request
        response = requests.get(url)

        return response.json()

    def get_users_list(self):
        # список совпадений
        request = '/users'
        url = self.root + request
        response = requests.get(url)

        return response.json()

    def get_pages_list(self):
        # список совпадений
        request = '/pages'
        url = self.root + request
        response = requests.get(url)

        return response.json()

    def get_total_rank_person(self, id):
        request = '/ranks/total/person/' + str(id)
        url = self.root + request
        response = requests.get(url)

        return response.json()

    def get_total_rank_person_on_site(self, id, site):
        request = '/ranks/total/person/' + str(id) + '/site/' + str(site)
        url = self.root + request
        response = requests.get(url)

        return response.json()


class Views:
    # Принимает string, рисует таблицу и заносит в нее данные

    def fill_table(self, results):
        table = window.ui.stats
        table.setColumnCount(len(results))
        table.clearContents()
        data_row = []
        labels = []
        for i in range(len(results)):
            labels.append(results[i].split(DELMITER)[0])
            data_row.append(results[i].split(DELMITER)[1:])  # отсекаем label

        table.setRowCount(len(data_row[0]))

        table.setHorizontalHeaderLabels(labels)
        table.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        table.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)

        # заливаем таблицу i строка, j столбец
        for i in range(len(data_row[0])):
            for j in range(len(results)):
                table.setItem(i, j, QTableWidgetItem(data_row[j][i]))
                table.setItem(i, j, QTableWidgetItem(data_row[j][i]))

# бесконечный цикл запускющий графическую оболочку
if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyWindow()
    window.show()
    sys.exit(app.exec_())
